package com.tmv.boot.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Attribute entity.
 */
@Entity
@Table(name = "attribute")
public class AttributeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long attribute_id;

    @Column(name = "attribute_name")
    private String attribute_name;

    /**
     * Instantiates a new Attribute entity.
     */
    public AttributeEntity() {
    }

    /**
     * Instantiates a new Attribute entity.
     *
     * @param attribute_name the attribute name
     */
    public AttributeEntity(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    @Override
    public String toString() {
        return "AttributeEntity{" +
                "attribute_id=" + attribute_id +
                ", attribute_name='" + attribute_name + '\'' +
                '}';
    }

    /**
     * Gets attribute id.
     *
     * @return the attribute id
     */
    public long getAttribute_id() {
        return attribute_id;
    }

    /**
     * Sets attribute id.
     *
     * @param attribute_id the attribute id
     */
    public void setAttribute_id(long attribute_id) {
        this.attribute_id = attribute_id;
    }

    /**
     * Gets attribute name.
     *
     * @return the attribute name
     */
    public String getAttribute_name() {
        return attribute_name;
    }

    /**
     * Sets attribute name.
     *
     * @param attribute_name the attribute name
     */
    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }
}