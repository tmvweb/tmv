package com.tmv.boot.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The type Category entity.
 */
    @Entity
    @Table(name = "category")
    public class CategoryEntity implements Serializable {

    private static final long serialVersionUID = -3109157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long category_id;

    @Column(name = "category_name")
    private String category_name;

    @Column(name = "parent_category_id")
    private long parent_category_id;

    /**
     * Instantiates a new Category entity.
     *
     * @param category_name      the category name
     * @param parent_category_id the parent category id
     */
    public CategoryEntity(String category_name, long parent_category_id) {
        this.category_name = category_name;
        this.parent_category_id = parent_category_id;
    }
}
