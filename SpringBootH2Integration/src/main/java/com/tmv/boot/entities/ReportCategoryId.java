package com.tmv.boot.entities;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
public class ReportCategoryId implements Serializable {

    private static final long serialVersionUID = -3109157732242241606L;

    @Id
    private int reportId;

    @Id
    private int categoryId;

    public ReportCategoryId(int reportId, int categoryId) {
        this.reportId = reportId;
        this.categoryId = categoryId;
    }

    public int getReportId() {
        return reportId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
