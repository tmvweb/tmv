package com.tmv.boot.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "reports")
public class ReportEntity implements Serializable {

    private static final long serialVersionUID = -3109157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long reportId;

    @Column(name = "report_name")
    private String reportName;



    @Column(name = "report_image")
    private String reportImage;

    @Column(name = "report_thumbnail_image")
    private String reportThumbnailImage;

    public ReportEntity(long reportId, String reportName, String reportImage, String reportThumbnailImage) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportImage = reportImage;
        this.reportThumbnailImage = reportThumbnailImage;
    }

    public long getReportId() {
        return reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public String getReportImage() {
        return reportImage;
    }

    public String getReportThumbnailImage() {
        return reportThumbnailImage;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setReportImage(String reportImage) {
        this.reportImage = reportImage;
    }

    public void setReportThumbnailImage(String reportThumbnailImage) {
        this.reportThumbnailImage = reportThumbnailImage;
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "reportId=" + reportId +
                ", reportName='" + reportName + '\'' +
                ", reportImage='" + reportImage + '\'' +
                ", reportThumbnailImage='" + reportThumbnailImage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryEntity that = (CategoryEntity) o;
        return reportId == reportId &&
                Objects.equals(reportName, this.reportName) &&
                Objects.equals(reportImage, this.reportImage) &&
                Objects.equals(reportThumbnailImage, this.reportThumbnailImage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(reportId, reportName, reportImage, reportThumbnailImage);
    }

}
