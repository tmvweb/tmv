package com.tmv.boot.entities;

import javax.persistence.*;

@Entity
public class ReportCategoryEntity {

    @EmbeddedId
    private ReportCategoryEntity  rpt_cat_key;

    @MapsId("reportId")
    @JoinColumn(name="reportId",referencedColumnName = "reportId")
    @ManyToOne
    ReportEntity report;

    @MapsId("categoryId")
    @JoinColumn(name="categoryId",referencedColumnName = "categoryId")
    @ManyToOne
    CategoryEntity category;

    public ReportCategoryEntity(ReportCategoryEntity rpt_cat_key, ReportEntity report, CategoryEntity category) {
        this.rpt_cat_key = rpt_cat_key;
        this.report = report;
        this.category = category;
    }

    public ReportCategoryEntity getRpt_cat_key() {
        return rpt_cat_key;
    }

    public void setRpt_cat_key(ReportCategoryEntity rpt_cat_key) {
        this.rpt_cat_key = rpt_cat_key;
    }

    public ReportEntity getReport() {
        return report;
    }

    public void setReport(ReportEntity report) {
        this.report = report;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }
}
