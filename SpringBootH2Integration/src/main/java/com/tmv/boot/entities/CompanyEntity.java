package com.tmv.boot.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Company")
public class CompanyEntity implements Serializable {

    private static final long serialVersionUID = -3209157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name= "company_id")
    private long company_id;

    @Column(name = "company_name")
    private String company_name;

    @Column(name = "company_desc")
    private String company_desc;

        public CompanyEntity() {
        }
        public CompanyEntity(String company_name, String company_desc) {
            this.company_name = company_name;
            this.company_desc = company_desc;
        }

        public long getCompany_id() {
            return company_id;
        }

        public void setCompany_id(long company_id) {
            this.company_id = company_id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getCompany_desc() {
            return company_desc;
        }

        public void setCompany_desc(String company_desc) {
            this.company_desc = company_desc;
        }
    }
