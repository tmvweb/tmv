package com.tmv.boot.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Customer entity.
 */
@Entity
@Table(name = "customer")
public class CustomerEntity implements Serializable {
 
    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
 
    @Column(name = "firstname")
    private String firstName;
 
    @Column(name = "lastname")
    private String lastName;

    /**
     * Instantiates a new Customer entity.
     */
    protected CustomerEntity() {
    }

    /**
     * Instantiates a new Customer entity.
     *
     * @param firstName the first name
     * @param lastName  the last name
     */
    public CustomerEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
 
    @Override
    public String toString() {
        return String.format("CustomerEntity[id=%d, firstName='%s', lastName='%s']", id, firstName, lastName);
    }
}
