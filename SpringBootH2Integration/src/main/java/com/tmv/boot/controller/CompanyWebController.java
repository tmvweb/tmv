package com.tmv.boot.controller;

import com.tmv.boot.entities.CompanyEntity;
import com.tmv.boot.entities.CompanyEntity;
import com.tmv.boot.entities.CompanyEntity;
import com.tmv.boot.models.CompanyDTO;
import com.tmv.boot.models.CompanyDTO;
import com.tmv.boot.repository.CompanyRepository;
import com.tmv.boot.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CompanyWebController {


    /**
     * The Category repository.
     */
    @Autowired
    CompanyRepository companyRepository;


    /**
     * Process string.
     *
     * @return the string
     */
    @RequestMapping(value= "/saveCompanies", method = RequestMethod.GET)
    public String process() {
        companyRepository.save(new CompanyEntity("Company_1", "ddesc1"));
        companyRepository.save(new CompanyEntity("Company_2", "desc1"));
        companyRepository.save(new CompanyEntity("Company_3", "desc3"));
        companyRepository.save(new CompanyEntity("Company_4", "desc4"));
        return "Done";
    }

    /**
     * Find all categories string.
     *
     * @return the string
     */
    @RequestMapping(value = "/findAllCompanies",method = RequestMethod.GET)
    public String findAllCompany() {
        String result = "";

        for (CompanyEntity companyEntity : companyRepository.findAll()) {
            result += companyEntity.toString() + "</br>";
        }
        return result;
    }


    @RequestMapping(value = "/saveCompanies", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> persistPerson(@RequestBody CompanyDTO companyDTO) {
        System.out.println(companyDTO.getCompany_name());
        CompanyEntity companyEntity = new CompanyEntity(companyDTO.getCompany_name(),companyDTO.getCompany_desc());
        companyRepository.save(companyEntity);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }



    /**
     * Find all attributes list.
     *
     * @return the list
     */
    @RequestMapping(value = "/getCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CompanyDTO> findAllComapany(){
        String result = "";
        List<CompanyDTO> newList = new ArrayList<>();
        for(CompanyEntity companyEntity : companyRepository.findAll()){
            CompanyDTO companyDTO = new CompanyDTO();
            companyDTO.setCompany_name(companyEntity.getCompany_name());
            companyDTO.setCompany_id(companyEntity.getCompany_id());
            companyDTO.setCompany_desc(companyEntity.getCompany_desc());
            newList.add(companyDTO);
        }
        return newList;
    }
}
