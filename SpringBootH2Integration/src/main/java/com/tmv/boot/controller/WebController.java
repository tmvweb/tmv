package com.tmv.boot.controller;

import com.tmv.boot.entities.CustomerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmv.boot.repository.CustomerRepository;

/**
 * The type Web controller.
 */
@RestController
public class WebController {
    /**
     * The Repository.
     */
    @Autowired
    CustomerRepository repository;

    /**
     * Process string.
     *
     * @return the string
     */
    @RequestMapping("/save")
    public String process(){
        repository.save(new CustomerEntity("Jack", "Smith"));
        repository.save(new CustomerEntity("Adam", "Johnson"));
        repository.save(new CustomerEntity("Kim", "Smith"));
        repository.save(new CustomerEntity("David", "Williams"));
        repository.save(new CustomerEntity("Peter", "Davis"));
        return "Done";
    }


    /**
     * Find all string.
     *
     * @return the string
     */
    @RequestMapping("/findall")
    public String findAll(){
        String result = "";

        for(CustomerEntity cust : repository.findAll()){
            result += cust.toString() + "</br>";
        }

        return result;
    }

    /**
     * Find by id string.
     *
     * @param id the id
     * @return the string
     */
    @RequestMapping("/findbyid")
    public String findById(@RequestParam("id") long id){
        String result = "";
        result = repository.findOne(id).toString();
        return result;
    }

    /**
     * Fetch data by last name string.
     *
     * @param lastName the last name
     * @return the string
     */
    @RequestMapping("/findbylastname")
    public String fetchDataByLastName(@RequestParam("lastname") String lastName){
        String result = "";
           
        for(CustomerEntity cust: repository.findByLastName(lastName)){
            result += cust.toString() + "</br>"; 
        }
           
        return result;
    }
}
