package com.tmv.boot.controller;

import com.tmv.boot.entities.CategoryEntity;
import com.tmv.boot.entities.ReportCategoryEntity;
import com.tmv.boot.entities.ReportEntity;
import com.tmv.boot.models.ReportDTO;
import com.tmv.boot.repository.CategoryReportRepository;
import com.tmv.boot.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Category web controller.
 */
@RestController
public class CategoryWebController {

    /**
     * The Category repository.
     */
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CategoryReportRepository categoryReportRepository;


    /**
     * Process string.
     *
     * @return the string
     */
    @RequestMapping("/saveCategories")
    public String process(){
        categoryRepository.save(new CategoryEntity("Category_1",1));
        categoryRepository.save(new CategoryEntity("Category_2",1));
        categoryRepository.save(new CategoryEntity("Category_3",3));
        categoryRepository.save(new CategoryEntity("Category_4",4));
        return "Done";
    }

    /**
     * Find all categories string.
     *
     * @return the string
     */
    @RequestMapping("/findAllCategories")
    public String findAllCategories(){
        String result = "";

        for(CategoryEntity categoryEntity : categoryRepository.findAll()){
            result += categoryEntity.toString() + "</br>";
        }
        return result;
    }

    @CrossOrigin
    @RequestMapping(value="/{categoryId}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReportDTO> getReportById(@PathVariable final Long categoryId){

        List<ReportCategoryEntity> reportCatList=categoryReportRepository.findAllByCategory_CategoryId(categoryId);
        List<ReportDTO> reportList= new ArrayList<>();
        for(ReportCategoryEntity reportCat : reportCatList){
            ReportEntity report=reportCat.getReport();
            ReportDTO repDTO = new ReportDTO(report.getReportId(),report.getReportName(),report.getReportImage(),report.getReportThumbnailImage());
            reportList.add(repDTO);
        }
        return reportList;
    }
}
