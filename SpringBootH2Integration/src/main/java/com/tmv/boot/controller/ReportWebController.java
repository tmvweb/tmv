package com.tmv.boot.controller;

import com.tmv.boot.entities.CompanyEntity;
import com.tmv.boot.entities.ReportEntity;
import com.tmv.boot.models.CompanyDTO;
import com.tmv.boot.models.ReportDTO;
import com.tmv.boot.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportWebController {

    @Autowired
    ReportRepository reportRepo;

    @CrossOrigin
    @RequestMapping(value="/{reportName}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReportDTO> getReportById(@PathVariable final String reportName){

        List<ReportDTO> reportList=new ArrayList<ReportDTO>();

        for(ReportEntity report : reportRepo.findByReportName(reportName)){
            ReportDTO repDTO = new ReportDTO(report.getReportId(),report.getReportName(),report.getReportImage(),report.getReportThumbnailImage());
            reportList.add(repDTO);
        }
        return reportList;
    }


}
