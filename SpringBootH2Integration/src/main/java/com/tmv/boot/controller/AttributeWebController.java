package com.tmv.boot.controller;

import com.tmv.boot.entities.AttributeEntity;
import com.tmv.boot.models.AttributeDTO;
import com.tmv.boot.repository.AttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Attribute web controller.
 */
@RestController
public class AttributeWebController {

    /**
     * The Attribute repository.
     */
    @Autowired
    AttributeRepository attributeRepository;

    /**
     * Process string.
     *
     * @return the string
     */
    @RequestMapping("/saveAttributeDummy")
    public String process(){
        attributeRepository.save(new AttributeEntity("attribute_1_val"));
        attributeRepository.save(new AttributeEntity("attribute_2_val"));
        attributeRepository.save(new AttributeEntity("attribute_3_val"));
        attributeRepository.save(new AttributeEntity("attribute_4_val"));
        return "Done";
    }

    /**
     * Persist person response entity.
     *
     * @param attributeDTO the attribute dto
     * @return the response entity
     */
    @RequestMapping(value = "/saveAttribute", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> persistPerson(@RequestBody AttributeDTO attributeDTO) {
        System.out.println(attributeDTO.getAttribute_name());
        AttributeEntity attributeEntity = new AttributeEntity(attributeDTO.getAttribute_name());
        attributeRepository.save(attributeEntity);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * Find all attributes list.
     *
     * @return the list
     */
    @RequestMapping(value = "/getAttributes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttributeDTO> findAllAttributes(){
        String result = "";
        List<AttributeDTO> newList = new ArrayList<>();
        for(AttributeEntity attributeEntity : attributeRepository.findAll()){
            AttributeDTO attributeDTO = new AttributeDTO();
            attributeDTO.setAttribute_name(attributeEntity.getAttribute_name());
            attributeDTO.setAttribute_id(attributeEntity.getAttribute_id());
            newList.add(attributeDTO);
        }
        return newList;
    }
}
