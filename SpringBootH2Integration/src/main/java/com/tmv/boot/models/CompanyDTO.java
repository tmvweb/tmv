package com.tmv.boot.models;

public class CompanyDTO {


    private long company_id;
    private String company_name;
    private String company_desc;

    public CompanyDTO(){

    }
    public CompanyDTO(long company_id, String company_name, String company_desc) {
        this.company_id = company_id;
        this.company_name = company_name;
        this.company_desc = company_desc;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_desc() {
        return company_desc;
    }

    public void setCompany_desc(String company_desc) {
        this.company_desc = company_desc;
    }
}
