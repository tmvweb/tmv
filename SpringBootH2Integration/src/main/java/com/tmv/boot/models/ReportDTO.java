package com.tmv.boot.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class ReportDTO {

    private long reportId;
    private String reportName;
    private String reportImage;
    private String reportThumbnailImage;

    public ReportDTO(long reportId, String reportName, String reportImage, String reportThumbnailImage) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportImage = reportImage;
        this.reportThumbnailImage = reportThumbnailImage;
    }

    public long getReportId() {
        return reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public String getReportImage() {
        return reportImage;
    }

    public String getReportThumbnailImage() {
        return reportThumbnailImage;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setReportImage(String reportImage) {
        this.reportImage = reportImage;
    }

    public void setReportThumbnailImage(String reportThumbnailImage) {
        this.reportThumbnailImage = reportThumbnailImage;
    }
}
