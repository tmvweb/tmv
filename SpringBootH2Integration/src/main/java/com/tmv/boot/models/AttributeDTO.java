package com.tmv.boot.models;

/**
 * The type Attribute dto.
 */
public class AttributeDTO {

    private String attribute_name;
    private long attribute_id;

    /**
     * Instantiates a new Attribute dto.
     */
    public AttributeDTO() {
    }

    /**
     * Gets attribute id.
     *
     * @return the attribute id
     */
    public long getAttribute_id() {
        return attribute_id;
    }

    /**
     * Sets attribute id.
     *
     * @param attribute_id the attribute id
     */
    public void setAttribute_id(long attribute_id) {
        this.attribute_id = attribute_id;
    }

    /**
     * Instantiates a new Attribute dto.
     *
     * @param attribute_name the attribute name
     */
    public AttributeDTO(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    /**
     * Gets attribute name.
     *
     * @return the attribute name
     */
    public String getAttribute_name() {
        return attribute_name;
    }

    /**
     * Sets attribute name.
     *
     * @param attribute_name the attribute name
     */
    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    @Override
    public String toString() {
        return "AttributeDTO{" +
                "attribute_name='" + attribute_name + '\'' +
                '}';
    }
}
