package com.tmv.boot.repository;

import com.tmv.boot.entities.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * The interface Category repository.
 */
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {
     //   List<CategoryEntity> findCategoryByCategory_id(long category_id);

        List<CategoryEntity> findAll();
    }

