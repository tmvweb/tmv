package com.tmv.boot.repository;

import com.tmv.boot.entities.CategoryEntity;
import com.tmv.boot.entities.ReportCategoryEntity;
import com.tmv.boot.entities.ReportCategoryId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryReportRepository extends CrudRepository<ReportCategoryEntity, ReportCategoryId> {

    List<ReportCategoryEntity> findAllByCategory_CategoryId(Long categoryId);
}
