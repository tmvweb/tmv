package com.tmv.boot.repository;


import com.tmv.boot.entities.ReportEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReportRepository  extends CrudRepository<ReportEntity, Long> {

    List<ReportEntity> findByReportName(String reportName);
}
