package com.tmv.boot.repository;

import com.tmv.boot.entities.CompanyEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

    public interface CompanyRepository extends CrudRepository<CompanyEntity, Long> {
        //   List<CompanyEntity> findCompanyByCompany_id(long company_id);

        List<CompanyEntity> findAll();
    }

