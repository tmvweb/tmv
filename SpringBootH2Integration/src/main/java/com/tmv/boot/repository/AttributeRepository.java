package com.tmv.boot.repository;

import com.tmv.boot.entities.AttributeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Attribute repository.
 */
@Repository
public interface AttributeRepository extends CrudRepository<AttributeEntity, Long> {

    List<AttributeEntity> findAll();
    //AttributeEntity findAttributeByAttribute_id(long attribute_id);
   // List<AttributeEntity> getAttributeByAttribute_name(String attribute_name);
}
