package com.tmv.boot.repository;

import java.util.List;

import com.tmv.boot.entities.CustomerEntity;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

/**
 * The interface Customer repository.
 */
@Repository
public interface CustomerRepository extends CrudRepository<CustomerEntity, Long>{
    /**
     * Find by last name list.
     *
     * @param lastName the last name
     * @return the list
     */
    List<CustomerEntity> findByLastName(String lastName);
    List<CustomerEntity> findAll();
} 