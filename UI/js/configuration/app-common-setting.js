﻿var WebLocalStorageSetting = {
    //OrganisationID: localStorage.getItem(value),
    SetLocalStorage: function (key, value) {
        localStorage.setItem(key, value);
    }
}


var apiSetting = {
    url: 'http://localhost:60797/api/',
    //url: 'http://services.humaraschool.com/api',
    controller: '',
    action: '',
    type: 'post',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    data: '',
    authorization:'bearer ' + localStorage.getItem("AUTHTOKEN"),
    //InstitutionBaseUrl: 'http://school.humaraschool.com/',
    InstitutionBaseUrl: 'http://localhost:52073/',
    InstitutionUrlPath:'/Dashboard/Index'
}, pageLoader = $('body');


$.fn.serializeFormJSON = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var AjaxRequest = function () {
    $.ajax({
        type: apiSetting.type,//default is post
        url: apiSetting.url + '/' + apiSetting.controller + '/' + apiSetting.action,
        contentType: apiSetting.contentType,
        data: apiSetting.data,
        beforeSend: apiSetting.beforeSend,
        success: apiSetting.success,
        error: apiSetting.error,
        complete: apiSetting.complete,
    });
}

var popup = {
    open: function (header, body, footer) {
        $('.sms-popup .sms-header-text').html(header);
        $('.sms-popup .sms-body').html(body);
        $('.sms-popup .sms-footer').html(footer);
        $('.sms-popup').fadeIn(300);
        $('.sms-popup').addClass('zoomIn');
        $('body').addClass('body-overflow');
    },
    close: function () {
        $('.sms-popup .sms-header-text').html('');
        $('.sms-popup .sms-body').html('');
        $('.sms-popup .sms-footer').html('');
        $('.sms-popup').fadeOut(500);
        $('.sms-popup').removeClass('zoomIn');
        $('body').removeClass('body-overflow');
    }
}