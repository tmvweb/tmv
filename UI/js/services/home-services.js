let homeDom = new HomeDom();
var HomeService = function () {
    this.OnPageLoad = function () {
        let _this = this;
        homeDom.LoadTemplate('mainBodyTemplate', mainBody, {});
        _this.LoadCartCount();
    }

    this.LoadCartCount = function () {
        console.log('calling');
        apiSetting.controller = 'Restaurant';
        apiSetting.action = 'GetSuggestions';
        apiSetting.type = 'Get';
        $.ajax({
            url: apiSetting.url + '/' + apiSetting.controller + '/' + apiSetting.action,
            type: apiSetting.type,
            contentType: false,
            processData: false,
            success: function (d) {
                // console.log(d);
                homeDom.LoadTemplate('cartTemplate', cartId, { Count: 4,LastCount:20 });
            },
            error: function () {
                console.log('Error, Integrate Pluggin Here')
            }
        });
    }
}